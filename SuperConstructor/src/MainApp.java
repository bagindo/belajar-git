
public class MainApp {

	public static void main(String[] args) {

		Doctor doctor1 = new Doctor();
		doctor1.name = "Marlong";
		doctor1.address = "Babakan Sari";
		doctor1.specialist = "THT";
		
		Doctor doctor2 = new Doctor("Joko", "Tegal", "Cardiologist");
		
		doctor1.greeting();
		System.out.println();
		
		doctor2.greeting();
	

	}

}
