
public class MainApp {

	public static void main(String[] args) {
		Person person1 = new Person();
		person1.name = "Andika";
		person1.address = "Bandung";
		
		Teacher teacher1 = new Teacher();
		teacher1.name = "Marsito";
		teacher1.address = "Kiaracondong";
		teacher1.subject = "Bahasa arab";
		
		Doctor doctor1 = new Doctor();
		doctor1.name = "Marlong";
		doctor1.address = "Babakan Sari";
		doctor1.specialist = "THT";
		
		Programmer programmer1 = new Programmer();
		programmer1.name = "Doeng";
		programmer1.address = "Soreang";
		programmer1.technology = "Javascript";
		
		person1.greeting();
		System.out.println();
		
		teacher1.greeting();
		System.out.println();
		
		doctor1.greeting();
		System.out.println();
		
		programmer1.greeting();
		
		
		

	}

}
